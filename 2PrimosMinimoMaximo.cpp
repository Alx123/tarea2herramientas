#include<iostream>

int primosMinimoMaximo(int N, int M);
int main(int argc, char **argv){
   const int N = atoi(argv[1]);
   const int M = atoi(argv[2]);
   primosMinimoMaximo(N,M);
   
   return 0;
}

int primosMinimoMaximo(int N, int M){
  int contador=0;
  for(int j=N; j<=M; j++){
    for(int i=2; i<j; i++){
      if(j%i!=0){
	contador=contador+1;
      }
    }
    if(contador==j-2){
      std::cout<<j<<std::endl;
    }
    contador=0;
  }
  return 1;
  
}
