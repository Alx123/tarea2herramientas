#include<iostream>

using namespace std;
void hanoi(int N,int origen, int aux, int destino){
  if(N==1){
    cout<<origen<<" -> "<<destino<<endl;
  }else{
    hanoi(N-1, origen,destino,aux);
    cout<<origen<<" -> "<<destino<<endl;
    hanoi(N-1,aux,origen,destino);
  }
}

int main(){
  int N;
  cout<<"Con cuantos discos quieres jugar: ";
  cin>>N;
  cout<<"Movimientos: "<<endl;;
  hanoi(N,1,2,3);
}
