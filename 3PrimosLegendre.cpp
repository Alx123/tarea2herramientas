#include<math.h>
#include<iostream>
#include<fstream>

using namespace std;
void primosLegendre(int M);

int main(int argc, char **argv){
  const int M = atoi(argv[1]);
  primosLegendre(M);
  
  return 0;
}

void primosLegendre(int M){
  ofstream myfile;
  myfile.open ("example.txt");

  int contador=0, cantidad=0; 
  double CantidadLegendre,Error;
  for(int j=1; j<=M; j++){
    for(int i=2; i<j; i++){
      if(j%i!=0){
	contador=contador+1;
      }
    }
    if(contador==j-2){
      cantidad++;
      //Formula de Legendre
      CantidadLegendre=j/(log(j)-1.08366);
      //Error
      Error=sqrt(pow((cantidad-CantidadLegendre)/cantidad,2))*100;
      
      
      myfile<<j<<" "<<cantidad<<"  "<<CantidadLegendre<<" "<<Error<<endl;
      //std::cout<<j<<" "<<cantidad<<"  "<<CantidadLegendre<<Error<<std::endl;
    }
    contador=0;
  }
  myfile.close();
}
