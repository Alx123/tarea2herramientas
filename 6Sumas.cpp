#include<iostream>
#include<math.h>
#include<cstdlib>
#include<fstream>

using namespace std;

typedef double REAL;

REAL suma1 (int N);
REAL suma2 (int N);
REAL suma3 (int N);
void graficar (int N);


int main(int argc, char **argv){
  const int N = atoi(argv[1]);
  graficar(N);
  return 0;
}


REAL suma1 (int N){
  REAL Suma1=0;
  for(int n=1; n<=2*N; n++){
    Suma1=Suma1+ ((n*1.0/(n+1))*pow(-1,n));
    
  }
  return Suma1;
}


REAL suma2 (int N){
  REAL Suma2=0, a=0, b=0; 
  for(int n=1; n<=N; n++){
    a=a+(2*n-1)*1.0/(2*n);
    b=b+(2*n)*1.0/(2*n+1);
    Suma2=b-a;
    
  }
  return Suma2;
}


REAL suma3 (int N){
  float Suma3=0;
  for(int n=1; n<=N;n++){
    Suma3=Suma3+1.0/((2*n)*(2*n+1));
    
  } 
  return Suma3;
}

void graficar (int N){
  ofstream myfile;
  double Error31, Error32;
  myfile.open ("Sumas.txt");
    for(int i=1; i<=N;i++){
      Error31=sqrt(pow((1.0*suma3(i)-suma1(i))/suma3(i),2))*100;
      Error32=sqrt(pow((1.0*suma3(i)-suma2(i))/suma3(i),2))*100;
      myfile<<i<<" "<<suma1(i)<<" "<<suma2(i)<<" "<<suma3(i)<<" "<<Error31<<" "<<Error32<<std::endl;
  }
 myfile.close();
}
 


